import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public identity;

  constructor(private _route: ActivatedRoute,
    private router: Router,
    private userService: UserService) { }

  ngOnInit() {
    this.identity = this.userService.getIdentity();
}

ngDoCheck() {
    this.identity = this.userService.getIdentity();
    // console.log("ide",this.identity)
}

  playAudio(){
    let audio = new Audio();
  audio.src = '../../../assets/music/entry.mp3';
  audio.load();
  audio.play();
  }

}
