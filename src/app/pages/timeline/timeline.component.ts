import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery' 

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {

  constructor(private router: Router) { 
    
  }

  ngOnInit() {
    $(document).ready(function(){
      var toggle = $('.navbar-toggle') 
      toggle.click(function(){
        var collapse = $('.sidebar')
        console.log("collapse",collapse);
        collapse.toggleClass('right');
        $('.navbar-toggle').toggleClass('indexcity');
        });
  });
    
  }

}
