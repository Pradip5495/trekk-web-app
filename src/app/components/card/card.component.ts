import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from 'src/app/services/post.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  // @Input() postArray:any;

  public token;
  public postData;
  public postArray;
  public userName;
public data;
  public identity;
  

  constructor(private userService: UserService,
    private postService: PostService,
    private router: Router) {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
     }

  ngOnInit() {
    this.identity = this.userService.getIdentity();
    console.log('create post',this.postArray)
    this.token = this.userService.getToken();
        console.log("tokem",this.token)
        this.getPosts(); 
  }

  getPosts(){
    this.postService.getPost(this.token).subscribe((response)=>{
      console.log("all posts",response)
      this.postData = response;

      this.postArray = this.postData.map((data1)=>{
        return data1;
      })
      this.data = this.postData.find((res)=>{
        this.userName = res.postUser;
      })
      
      
    })
  }
  



  
  

  
  



}
