import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from 'src/app/services/post.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public identity;
  public id;
  public ids;
  public token;
  public postArray;
  public user;

  constructor(private postService: PostService,
    private userService: UserService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.identity = this.userService.getIdentity();
    // this.ids = JSON.stringify(this.identity.id);
    // console.log("id",this.id)
    this.token = this.userService.getToken();
    
    this.route.params.subscribe(
      params => {
           this.id = params['id'];
          
          // this.getCounter(id);
      }
  );
  this.getUserById(this.id);
  this.getPosts(this.id);
  }

  getPosts(id){
    this.postService.getPostById(id).subscribe((response)=>{
      console.log("all posts",response)
      this.postArray = response;  
    })
  }

  getUserById(id){
    this.userService.getUserById(id).subscribe((response)=>{
      console.log("singleuser",response);
      this.user = response;
    })
  }

}
