import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-find-friends',
  templateUrl: './find-friends.component.html',
  styleUrls: ['./find-friends.component.scss']
})
export class FindFriendsComponent implements OnInit {
  public users;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getAllUsers();
  }
  
  getAllUsers(){
    this.userService.getAllUsers().subscribe((response)=>{
      this.users = response;
    })
  }
}
