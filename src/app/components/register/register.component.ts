import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as AnyControl from 'anycontrol';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  registerForm: FormGroup;
  public submitted = false;

  anyControl: {
    start: Function;
    addCommand: Function;
    recognition: any;
  };
  speechRecognitionList: any;

  constructor(private router: Router,private authService: AuthService,private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['', [Validators.required, Validators.pattern('^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[@#$&!]).{6,}$')]],
    })

    this.anyControl = new AnyControl.default();
      this.anyControl.addCommand('register please', () => {
        this.onSubmit();
      });
      // this.anyControl.recognition.lang = 'en-IND';
      this.anyControl.start();
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if(this.registerForm.valid){
    const formData = this.registerForm.value;
    this.authService.register(formData).subscribe(
      data => {
        // console.log("data",data.body.message);
        if(this.isSuccessful = true){
          this.nextPage();
        }
        if(data.status == 400){
          alert("email already exists")
        }
        this.isSignUpFailed = false;
       
        
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );}
  }

  nextPage(){
    this.router.navigate([`/login`]);
  }
}

