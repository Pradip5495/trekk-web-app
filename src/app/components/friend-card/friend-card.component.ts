import { Component, Input, OnInit } from '@angular/core';
import { Follow } from 'src/app/models/follow';
import { FollowService } from 'src/app/services/follow.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-friend-card',
  templateUrl: './friend-card.component.html',
  styleUrls: ['./friend-card.component.scss']
})
export class FriendCardComponent implements OnInit {
  @Input() user:any;
  public identity;
  public token;
  public followers=[];
  public followings=[];

  constructor(private userService: UserService,
    private followService: FollowService) { }

  ngOnInit() {
    this.identity = this.userService.getIdentity();
    this.token = this.userService.getToken();
    console.log("users",this.user)
    this.getCount()
  }

  followUser(followed){
    console.log(followed);
    var follow = new Follow(this.identity.id, followed);
    this.followService.addFollow(this.token,follow).subscribe((response)=>{
      console.log("gvhg",response);
    })
  }

  getCount(){
    
    this.followers = this.user.follower
    this.followings = this.user.following
    // if(this.followings == null){
    //   return 0
    // }else{
    //   this.followings
    // }
    
  }

}
