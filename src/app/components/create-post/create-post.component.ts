import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { PostService } from 'src/app/services/post.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent implements OnInit {
  public identity;
  postForm: FormGroup;
  public id;
  public data;
  public token;

  constructor(private userService: UserService,
    private router: Router, private formBuilder: FormBuilder,
    private postService: PostService) { }

  ngOnInit() {
    this.identity = this.userService.getIdentity();
    // console.log('user',this.identity)

    this.token = this.userService.getToken();
        // console.log("tokem",this.token)
    this.postForm = this.formBuilder.group({
      caption: ['', [Validators.required]],
      createdAt: ['', Validators.required],
    })

    
    
    this.id = JSON.stringify(this.identity.id);
    console.log("id",this.id)
  }

  onSubmit(){
    const formData = this.postForm.value;
    this.postService.addPost(this.token,this.id,formData).subscribe((response)=>{
      console.log("response",response);
      window.location.reload();
    })
  }

}
