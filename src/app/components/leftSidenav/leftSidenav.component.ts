import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import { UserService } from 'src/app/services/user.service';
import { PostComponent } from '../post/post.component';

@Component({
  selector: 'app-leftSidenav',
  templateUrl: './leftSidenav.component.html',
  styleUrls: ['./leftSidenav.component.scss']
})
export class LeftSidenavComponent implements OnInit {
  public identity;
  public ids;
  public follower=[];
  public following=[];
  public singlePost=[];

  constructor(private userService: UserService,private postService:PostService) { }

  ngOnInit() {

    this.identity = this.userService.getIdentity();
    console.log("incompo",this.identity)
    this.ids = JSON.stringify(this.identity.id);
    this.getSingleUser();
    this.getPosts();
  }

  getSingleUser(){
    this.userService.getSingleUserById(this.ids).subscribe((response)=>{
      
      this.follower = response.follower;
      
      
      this.following = response.following;
     
    })
  }

  getPosts(){
    this.postService.getPostbySidebar(this.ids).subscribe((response)=>{
      this.singlePost = response;  
    })
  }

  openNav() {
    document.getElementById("myNav").style.width = "100%";
  }

}
