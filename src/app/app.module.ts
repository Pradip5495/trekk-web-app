import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { authInterceptorProviders } from './helpers/auth.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './pages/navbar/navbar.component';
import { LeftSidenavComponent } from './components/leftSidenav/leftSidenav.component';
import { TimelineComponent } from './pages/timeline/timeline.component';
import { PostComponent } from './components/post/post.component';
import { CreatePostComponent } from './components/create-post/create-post.component';
import { CardComponent } from './components/card/card.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RightSidenavComponent } from './components/rightSidenav/rightSidenav.component';
import { OnlineFriendsComponent } from './components/online-friends/online-friends.component';
import { FriendCardComponent } from './components/friend-card/friend-card.component';
import { FindFriendsComponent } from './components/find-friends/find-friends.component';
import { UserGuard } from './services/user.guard';
import { LoginGuard } from './services/login.guard';

declare var $: any;




@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    NavbarComponent,
    LeftSidenavComponent,
    TimelineComponent,
    PostComponent,
    CreatePostComponent,
    CardComponent,
    ProfileComponent,
    RightSidenavComponent,
    OnlineFriendsComponent,
    FriendCardComponent,
    FindFriendsComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    
  ],
  providers: [authInterceptorProviders,UserGuard,LoginGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
