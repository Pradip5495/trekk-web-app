import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FindFriendsComponent } from './components/find-friends/find-friends.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RegisterComponent } from './components/register/register.component';
import { TimelineComponent } from './pages/timeline/timeline.component';
import { LoginGuard } from './services/login.guard';
import { UserGuard } from './services/user.guard';




const routes: Routes = [
  {
    path: "",
    component: LoginComponent,
     
  },
  {
    path: "register",
    component: RegisterComponent,
    canActivate:[LoginGuard]
  },
  {
    path:"login",
    component: LoginComponent,
    canActivate:[LoginGuard]
  },
  {
    path:"timeline",
    component: TimelineComponent,
    canActivate:[UserGuard]
  },
  {
    path:"profile/:id",
    component: ProfileComponent,
    canActivate:[UserGuard]
  },
  {
    path:"all-users",
    component:FindFriendsComponent,
    canActivate:[UserGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
