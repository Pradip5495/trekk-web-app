import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FollowService {

constructor(private http: HttpClient) { }

addFollow(token, follow): Observable<any> {
  console.log("follow from service",follow)
  let params = JSON.stringify(follow);
  let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                 .set('Authorization', token);

  return this.http.post(`${environment.API_ENDPOINT}/follow/`, params, {headers: headers});
}
}
