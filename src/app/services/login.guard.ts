import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {UserService} from './user.service';

@Injectable()
export class LoginGuard implements CanActivate {

    constructor(
        private router: Router,
        private userService: UserService
    ) {

    }

    canActivate() {
        let identity = this.userService.getIdentity();
        console.log(identity);
        
        if (!!identity && !(identity.roles == 'ROLE_USER' || identity.roles == 'ROLE_ADMIN')) {
            return true;
        } else {
            this.router.navigate(['/timeline']);
            return false;
        }
    }
}