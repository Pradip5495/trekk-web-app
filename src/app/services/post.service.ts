import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class PostService {

constructor(private http: HttpClient) { }

getPost(token): Observable<any>{
  let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                       .set('Authorization', token);
  return this.http.get(`${environment.API_ENDPOINT}/post/allposts`,{headers: headers}

  );
  
}

getPostById(id): Observable<any> {
  return this.http.get(`${environment.API_ENDPOINT}/post/` + id + `/posts`, httpOptions);
}

getPostbySidebar(id): Observable<any> {
  let params = JSON.parse(id);
  console.log("params",params)


  return this.http.get(`${environment.API_ENDPOINT}/post/` + params + `/posts`, httpOptions);
}

addPost(token, id,data): Observable<any> {
  let params = JSON.parse(id);
  console.log("params",params)
  let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                 .set('Authorization', token);

  return this.http.post(`${environment.API_ENDPOINT}/post/` + params + `/add`,data, {headers: headers});
}

}
