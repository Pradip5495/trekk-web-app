import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public identity;
  public token;

constructor(private http: HttpClient) { }

getIdentity() {
  let identity = JSON.parse(localStorage.getItem('auth-user'));

  if (identity != "undefined") {
      this.identity = identity;
  } else {
      this.identity = null;
  }
  // console.log("this.identity",this.identity)
  return this.identity;
}

getToken() {
  let token = localStorage.getItem('auth-token');

  if (token != "undefined") {
      this.token = token;
  } else {
      this.token = null;
  }

  return this.token;
}

getAllUsers():Observable<any>{
  return this.http.get(`${environment.API_ENDPOINT}/user/all`,httpOptions
  ); 
}

getUserById(id):Observable<any>{
  return this.http.get(`${environment.API_ENDPOINT}/user/`+ id ,httpOptions
  ); 
}

getSingleUserById(id):Observable<any>{
  let params = JSON.parse(id);
  return this.http.get(`${environment.API_ENDPOINT}/user/`+ params ,httpOptions
  ); 
}

}
