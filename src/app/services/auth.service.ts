import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

constructor(private http:HttpClient) { }

login(credentials): Observable<any> {
  return this.http.post(`${environment.API_ENDPOINT}/api/auth/signin`,
  credentials,
   httpOptions);
}

register(user): Observable<any> {
  return this.http.post(`${environment.API_ENDPOINT}/api/auth/signup`, user
  ,httpOptions);
}

}
